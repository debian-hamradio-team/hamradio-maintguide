Version Control System
======================

The Debian Hamradio Maintainers team has a project on `Salsa
<http://salsa.debian.org/>`_. You are encouraged to use this for managing git
repositories for the source of your team packages. If you do not currently have
an account on Salsa or have not requested to be added to the project, you will
need to do that before you can use this facility. To request to be added to the
project, send an email to the `mailing list
<https://lists.debian.org/debian-hams/>`_. In order to be able to push to the
repositories on Salsa, you will need to add an SSH key to your Salsa account.

Anonymous access to git repositories
------------------------------------

Anonymous access to git repositories is possible at the following URLs:

* https://salsa.debian.org/debian-hamradio-team/<pkg-name>.git (Preferred for the
  Vcs-Git field)

Anonymous access to a web VCS browser is also available at the following URL:

* https://salsa.debian.org/debian-hamradio-team/<pkg-name>.git

You can also view an `index of all team git repositories
<https://salsa.debian.org/debian-hamradio-team>`_.

Repository layout
-----------------

The layout of the Git repository is important as we have QA tools that scan our
Git repositories for information. The recommended layout for repositories is
described by `DEP-14 <http://dep.debian.net/deps/dep14/>`_, which is
implemented by the ``git-buildpackage`` tool. Examples of this tool in use are
given below.

Importing an existing source package
------------------------------------

You will need ``git-buildpackage`` installed on your local machine before
following these steps.

If your package is not currently tracked with git, you can import the sources
from a source package.

Begin by logging into Salsa and creating the new repository.
To configure the Salsa repository you've created you can use the script at
`<https://salsa.debian.org/debian-hamradio-team/hamradio-maintguide/tree/master/salsa>`_.

If you have the source package locally, you can skip this next step. If not,
you will need to download the source package from the Debian archives:
::

    cd /tmp
    apt-get source <pkg-name>

Then you need to create a local git repository on your machine and import the
source package:
::

    mkdir <pkg-name>
    cd <pkg-name>
    git init
    gbp import-dsc --pristine-tar /tmp/<pkg-name>*.dsc

Finally, in your local git repository, add the remote and push all branches and
tags:
::

    git remote add origin git+ssh://salsa.debian.org/debian-hamradio-team/<pkg-name>.git
    git push origin --all
    git push origin --tags

Please remember to update your ``Vcs-Git`` and ``Vcs-Browser`` fields in
``debian/control`` to match the URLs shown above for anonymous access.

Importing an existing git repository
------------------------------------

You will need ``git-buildpackage`` installed on your local machine before
following these steps.

If you've previously had a git repository elsewhere for your package, you can
import it quite easily.

Begin by logging into Salsa and creating the new repository.
To configure the Salsa repository you've created you can use the script at
`<https://salsa.debian.org/debian-hamradio-team/hamradio-maintguide/tree/master/salsa>`_.

Then in your local git repository, add the remote and push all branches and
tags:
::

    git remote remove origin
    git remote add origin git+ssh://salsa.debian.org/debian-hamradio-team/<pkg-name>.git
    git push origin --all
    git push origin --tags

This repository can now be cloned using git-buildpackage or you can continue to
work in your local repository pushing changes to the new origin.

Please remember to update your ``Vcs-Git`` and ``Vcs-Browser`` fields in
``debian/control`` to match the URLs shown above for anonymous access.

Creating a new git repository for a new package
-----------------------------------------------

You will need ``git-buildpackage`` installed on your local machine before
following these steps.

Begin by logging into Salsa and creating the new repository.

On your machine, download the upstream source tarball and keep it somewhere
safe. You will need a tarball to import, so if upstream distributes source as a
zip or other type of archive, you will need to repack it as a tarball.

Create a local git repository on your machine and import the tarball like so:
::

    mkdir <pkg-name>
    cd <pkg-name>
    git init
    gbp import-orig --pristine-tar /path/to/upstream/source.tar.gz

You'll now find that the upstream source has been imported into your git
repository.

To configure the Salsa repository you've created you can use the script at
`<https://salsa.debian.org/debian-hamradio-team/hamradio-maintguide/tree/master/salsa>`_.


When you are ready to push your changes to Salsa, add the remote and push all
branches and tags:
::

    git remote add origin git+ssh://salsa.debian.org/debian-hamradio-team/<pkg-name>.git
    git push --all
    git push --tags

Updating a package already in the team git
------------------------------------------

You can update an existing Debian package in git to the latest upstream with
one of the following commands:

::

    gbp import-orig --pristine-tar --uscan # will use the watch file to locate
                                           # the latest upstream
    gbp import-orig --pristine-tar --download <URL> # will download a tarball
                                                    # from the URL before
                                                    # importing
    gbp import-orig --pristine-tar /path/to/orig.tar.gz # import from local
                                                        # tarball

Testing package builds with git-buildpackage
--------------------------------------------

While building your package with git-pbuilder as described below will help you
to test building your package on a clean system and check your build
dependencies, it can take a long time to install the build dependencies every
time. You can build your package in the directory on your development system
while debugging build problems.
::

    gbp buildpackage -us -uc

.. note:: The ``-us -uc`` options will be passed to ``dpkg-buildpackage`` and
          disable the automatic signing of the package. There is no need to
          sign the built packages when debugging.

The .dsc, .changes, .orig.tar.gz, .debian.tar.xz and .deb files will be placed
in the parent directory to your git repository.

Using git-buildpackage and git-pbuilder for building packages
-------------------------------------------------------------

``git-pbuilder`` builds your package in a chroot which ensures that your
package has the correct package dependencies and does not depend on any files
outside of the repository. It is included in the ``git-buildpackage`` package.

The first time you use git-pbuilder, you'll need to create a local unstable
chroot for the builds to be performed in. You will also need to make sure
cowbuilder is available on your system.
::

    sudo apt-get install cowbuilder
    sudo git-pbuilder create

In order to build your package from the git repository, run:
::

    cd /path/to/repository
    gbp buildpackage --git-pbuilder

Final build and upload
----------------------

If your package is ready for upload, you can update the changelog, perform a
final build with the updated changelog, sign your package using debsign and
then upload:
::

    dch -r # this will set the distribution to unstable
    gbp buildpackage --git-pbuilder --git-tag
    cd ../
    debsign <pkg>*.changes
    dput <target> <pkg>*.changes

.. note:: You may choose to do the final build without ``git-pbuilder``, in
          which case you can omit the ``--git-pbuilder`` argument from the
          call to ``gbp buildpackage``. The .dsc and .changes files will also
          be signed during the build so you do not have to use ``debsign`` if
          you are not using ``git-pbuilder`` for the final build.

If you're not a Debian Maintainer or Debian Developer, you will not be able to
upload directly into the Debian archives. See the "Publishing your packages"
section of the `mentors.debian.org introduction
<http://mentors.debian.net/intro-maintainers>`_ to learn how to upload your
package there and then send a mail to the mailing list requesting sponsorship
for your package.

Backports and updates to stable distributions
---------------------------------------------

Backports and updates to stable distributions should be managed in a separate
branch in the Git repositories. Backports should be managed in the branch with
the name of the stable distribution, e.g. the branch containing backports for
the current stable distribution should be named "bullseye". Proposed updates for
the stable distributions should be managed in the branch with the name of the
stable distribution with "-pu" appended, e.g. the branch containing proposed
updates for the current stable distribution should be named "bullseye-pu".

In order to use git-buildpackage to build the alternative branch, use the
--git-debian-branch argument:
::

    gbp buildpackage --git-pbuilder --git-debian-branch=bullseye

Tracking commit notifications
-----------------------------

Commit notifications are displayed in the `#debian-hams
<irc://irc.debian.org/#debian-hams>`_ IRC channel on irc.debian.org and can
also be found on the `debian-hams@lists.debian.org
<https://lists.debian.org/debian-hams>`_
mailing list.

Further Reading
---------------

For more information on using Git for packaging, see `the Git packages pages on
the wiki <http://wiki.debian.org/GitPackaging>`_.

