Packaging
=========

Newcomer guidelines
-------------------

Newcomers may try to start off with a ``debian/`` directory and move files
around until they have a working binary ``.deb``. This sort of package will
never enter the official Debian archives however. The only way to get your
package into the Debian archives is through a proper source package that
complies with `Debian Policy <https://www.debian.org/doc/debian-policy/>`_. See
the `Debian New Maintainers Guide
<http://www.debian.org/doc/manuals/maint-guide/>`_ for help on getting started
with packaging. You may also find our `salsa script
<https://salsa.debian.org/debian-hamradio-team/hamradio-maintguide/tree/master/salsa>`_
useful for configuring new repositories if you have an account on
salsa.debian.org. 

Announcing intention to package
-------------------------------

If the package you would like to work on is new to Debian, you should announce
your intention to create the package. You do this by filing a WNPP bug report.
Please add an X-Debbugs-CC header pointing to the Debian Hamradio Maintainers
mailing list when you submit the bug.

Team Policy
-----------

It will be expected that you have read the `Debian Policy`_, `Developers Reference
<http://www.debian.org/doc/developers-reference/>`_ and the `New Maintainers
Guide <http://www.debian.org/doc/maint-guide/>`_.

In order to harmonise the packages within the Debian Hamradio Maintainers team,
please adhere to the following additional team policy:

``debian/control``
^^^^^^^^^^^^^^^^^^

The following considerations should be made when creating or modifying a
package's control file.

+-------------------+--------------------------------------------------------------------------------------+
| Section           | Should be ``hamradio`` for the source package in most cases. In                      |
|                   | some cases ``science`` will be more appropriate.                                     |
+-------------------+--------------------------------------------------------------------------------------+
| Priority          | Should be ``optional`` unless forbidden by the Debian Policy                         |
|                   | (see `§2.5                                                                           |
|                   | <http://www.debian.org/doc/debian-policy/ch-archive.html#s-priorities>`_).           |
+-------------------+--------------------------------------------------------------------------------------+
| Maintainer        | Maintainer should be ``Debian Hamradio Maintainers <debian-hams@lists.debian.org>``. |
|                   | Please subscribe to this list                                                        |
|                   | if you list yourself in the Uploaders: field of one of                               |
|                   | the team's packages.                                                                 |
+-------------------+--------------------------------------------------------------------------------------+
| Uploaders         | Please add yourself as an uploader when you have a significant interest in a package.|
|                   | Being Uploader means that you are expected to be responsive to bug reports and act   |
|                   | as a primary contact for the package. For more occasional works, you can do a team   |
|                   | upload. There must be at least one named person in the Uploaders field for each team |
|                   | package.                                                                             |
+-------------------+--------------------------------------------------------------------------------------+
| Standards-Version | Please always use the latest unless there are concerns for backporting. If no        |
|                   | changes are needed, please indicate this fact in the changelog, and increment        |
|                   | the value of the field.                                                              |
+-------------------+--------------------------------------------------------------------------------------+
| Homepage          | Should be documented whenever possible.                                              |
+-------------------+--------------------------------------------------------------------------------------+
| Vcs-*             | Please use the following template when using the team's Git repositories on Salsa:   |
|                   | ::                                                                                   |
|                   |                                                                                      |
|                   |     Vcs-Browser: https://salsa.debian.org/debian-hamradio-team/<pkg>                 |
|                   |     Vcs-Git: https://salsa.debian.org/debian-hamradio-team/<pkg>.git                 |
+-------------------+--------------------------------------------------------------------------------------+

It is a good idea to check the syntax of the control file with *Config::Model*.
This tool is also capable of reformatting the control file to a unified format
(although the reformatting currently appears to only work if there are errors
in the file to be fixed).
::

    sudo apt-get install cme libconfig-model-dpkg-perl libconfig-model-itself-perl
    cme check dpkg-control # checks the control file syntax
    cme fix dpkg-control # fixes errors if possible

``debian/copyright``
^^^^^^^^^^^^^^^^^^^^

We use the machine-readable format for the debian/copyright file. The Source
field does not need to contain the full URL to the particular version that is
being packaged, since this can be determined by the uscan program with the
debian/watch file. Please list yourself in the ``Files: debian/*``
section if you think that your contributions are not trivial and therefore
subjected to copyright. Please choose a license that is compatible with the
program you package. You can also use “same as if it were in the public domain”
or “same as the packaged program itself”.

To create some reasonable skeleton for a debian/copyright file you can try the
following:
::

   sudo apt-get install devscripts cdbs
   licensecheck --copyright -r `find -type f` | /usr/lib/cdbs/licensecheck2dep5 > debian/copyright

To verify the correct syntax of the debian/copyright file you can use
*Config::Model* again:
::

   cme check dpkg-copyright

``debian/changelog``
^^^^^^^^^^^^^^^^^^^^

Packages hosted in our Git repositories on Alioth that have been modified but
not uploaded must use ``UNRELEASED`` as a distribution name. QA tools will use
this to identify packages that are being worked on but for which uploads are
not yet ready.

``debian/README.source``
^^^^^^^^^^^^^^^^^^^^^^^^

This file is recommended by the Policy (`§4.14
<http://www.debian.org/doc/debian-policy/ch-source.html#s-readmesource>`_) from
version 3.8.0 for documenting source package handling. Please follow the
recommendation.

``debian/README.test``
^^^^^^^^^^^^^^^^^^^^^^

This file was `recommended by the Security team
<https://lists.debian.org/debian-devel-announce/2011/01/msg00006.html>`_ for
describing to others than the regular maintainer how the package's
functionality can properly be tested. If it is not obvious how to test the
functionality of your package, please consider including this file.

``debian/source/format``
^^^^^^^^^^^^^^^^^^^^^^^^

This file should contain “3.0 (quilt)” in order to use this source format. Other
formats should be avoided unless they bring a specific advantage.

Debhelper
^^^^^^^^^

Debhelper uses compatibility levels to control the behaviour of its commands.
We currently recommend to use the level 13 which is available in both stable
(bullseye) and as a backport in oldstable (buster). However, there is no
urgent need to touch packages only because it has an older Debhelper version.

It is strongly recommended to use the short dh notation in debian/rules files
which makes code factorisation very simple and easy to understand the packaging
for other members of the team. Even complex packaging becomes quite transparent
this way.

Team Uploads
^^^^^^^^^^^^

If you have contacted the uploaders listed for a package and they are
unresponsive, or if you have been asked by the uploaders to perform a team
upload, you may perform an upload without adding yourself to the uploaders
field. Team uploads are distinguished from NMUs by adding "* Team upload" as
the first changelog entry, and this is supported by debchange (--team).

For more information about team uploads, you can see the page about `team
uploads <https://wiki.debian.org/TeamUpload>` on the Wiki.

``/var/ax25``
^^^^^^^^^^^^^

Many packet radio applications use ``/var/ax25`` as a location for storing
operational state. This directory is not listed in the filesystem hierarchy
standard but this has been the directory used in Debian for decades and so
has become a de-facto standard. Unless the Debian technical committee decide
otherwise (no one has asked at the time of writing) use ``/var/ax25`` in
preference to ``/var/lib/ax25``. The team will consider use of ``/var/lib/ax25``
as a bug in Debian packages.
