QA Tools
========

A number of QA tools are available to the team to help track our packages. You
can also use most of these tools to track the packages that you are an uploader
on.

Debian Developer's Package Overview
-----------------------------------

Debian Developer's Package Overview (DDPO) is a web application offering a
customizable overview of all packages of a given maintainer.

* Link: `DDPO for the Debian Hamradio Maintainers
  <http://qa.debian.org/developer.php?login=debian-hams%40lists.debian.org&comaint=yes>`_

Debian Maintainer Dashboard
---------------------------

The maintainer dashboard exposes information about teams or maintainers'
packages. It intends to help answering the question "I have a few hours for
Debian, what should I do now?"

* Link: `DMD for the Debian Hamradio Maintainers
  <https://udd.debian.org/dmd/?email1=debian-hams%40lists.debian.org&email2=&email3=&packages=&ignpackages=&format=html#todo>`_

Debian Package Tracker
----------------------

The Debian Package Tracker service is a collection of scripts that gather
information about the team's packages. It allows you to see a bird's eye view
the health of many packages, instantly realizing where work is needed.  It also
allows you view the details of an invidual package, including links to open
bugs, the version in different Debian releases, news about recent uploads,
links to buildd logs, the packaging repo, and more.

* Link: `DPT for the Debian Hamradio Maintainers
  <https://tracker.debian.org/teams/debian-hamradio-maintainers/>`_

Lintian
-------

Lintian dissects Debian packages and tries to find bugs and policy violations.
It contains automated checks for many aspects of Debian policy as well as some
checks for common errors.

* Link: `Lintian reports for the Debian Hamradio Maintainers
  <https://lintian.debian.org/maintainer/debian-hams@lists.debian.org.html>`_

