Introduction
============

The Debian Hamradio Maintainers team collaborates on maintenance of
amateur-radio related packages for Debian. Team members may also maintain
radio-related packages independently using the team's infrastructure; there is
no obligation to share all radio-related package development.

Anyone is welcome to join; please contact the mailing list. New team members
may help with the existing packages or create new packages as they wish. You do
not need to be a Debian Maintainer or Debian Developer in order to contribute
to packages. See below for more information on getting involved.

Joining the team as a packager
------------------------------

To join the team you should join our mailing list at a bare minimum. In order
to use our Git repositories, you will need an Alioth account and this is very
much encouraged. If possible, you are also encouraged to join our IRC channel.

To join the mailing list, visit `the subscription page
<https://lists.debian.org/debian-hams/>`_ and enter your email address to
subscribe. You will have to reply to an email in order to confirm your
subscription.

To sign up for an Salsa account, visit `the Salsa registration page
<https://signup.salsa.debian.org/>`_ and register. You can then visit the `project
page for the Hamradio Maintainers team
<https://salsa.debian.org/debian-hamradio-team/>`_ and request to be added to
the team.

Our IRC channel is `#debian-hams <irc://irc.debian.org/#debian-hams>`_ on
irc.debian.org.

The following should be considered essential reading for anyone wishing to
participate in packaging within the team:

* `Debian Policy <http://www.debian.org/doc/debian-policy/>`_
* `Developers Reference <http://www.debian.org/doc/developers-reference/>`_
* `New Maintainers Guide <http://www.debian.org/doc/maint-guide/>`_
* Debian Hamradio Maintainers Guide (this document)

Assisting the team with translations
------------------------------------

If you speak a language other than English, you can contribute right away with
translations of package descriptions at the `Debian Description Translation
Project <https://www.debian.org/international/l10n/ddtp>`_.

